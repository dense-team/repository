<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 2.7.2016
 * Time: 13:30
 */

namespace Dense\Repository\Middleware;

use \Dense\Repository\Db\Profiler\Profiler;

class ProfilerMiddleware
{
    public function handle($request, \Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // database profiling
        if (!$request->ajax()) {
            $profiler = Profiler::getInstance();
            $profiler->printToConsole();
        }
    }
}
