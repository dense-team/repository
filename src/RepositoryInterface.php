<?php

namespace Dense\Repository;

interface RepositoryInterface
{
    public function find($id);

    public function all();

    public function load($limit = null, $offset = null);

    public function save($model);

    public function create($model);

    public function modify($model);

    public function remove($model);
}
