<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 15.2.2016
 * Time: 7:55
 */

namespace Dense\Repository\Db;

use Dense\Repository\Db\Statement\Statement;

abstract class TableAbstract
{
    use \Dense\Repository\Db\Adapter\AdapterTrait;
    use \Dense\Repository\Db\Statement\StatementTrait;

    use \Dense\Repository\Result\ResultTrait;

    /**
     * @var
     */
    protected $driver;

    public function __construct($modelClass, $resultType = null)
    {
        $this->initResult($modelClass, $resultType);
    }

    /**
     * @return mixed
     */
    protected function getDriver()
    {
        return $this->driver;
    }

    /* Query building methods */

    /**
     * @return array
     * @throws \Exception
     */
    public function buildSelect()
    {
        $this->addSelectDefaults();

        return $this->buildQuery(Statement::STMT_SELECT);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function buildUpdate(array $params)
    {
        $this->getStatement()
            ->addBindings($params);

        return $this->buildQuery(Statement::STMT_UPDATE, array_keys($params));
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function buildInsert(array $params)
    {
        $this->getStatement()
            ->addBindings($params);

        return $this->buildQuery(Statement::STMT_INSERT, array_keys($params));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function buildDelete()
    {
        return $this->buildQuery(Statement::STMT_DELETE);
    }

    /**
     * @param string $type
     * @param array $columns
     * @return array
     * @throws \Exception
     */
    protected function buildQuery($type, array $columns = [])
    {
        $query = null;

        switch (strtolower($type)) {

            case Statement::STMT_SELECT:
                $query = $this->getStatement()
                    ->makeSelect();

                break;

            case Statement::STMT_UPDATE:
                $query = $this->getStatement()
                    ->makeUpdate($columns);

                break;

            case Statement::STMT_INSERT:
                $query = $this->getStatement()
                    ->makeInsert($columns);

                break;

            case Statement::STMT_DELETE:
                $query = $this->getStatement()
                    ->makeDelete();

                break;
        }

        if ($query === null) {
            throw new \Exception('Invalid statement type.');
        }

        $binds = $this->getStatement()
            ->makeBindings();

        $this->destroyStatement();

        return [$query, $binds];
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->getStatement()->setOffset($offset);

        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->getStatement()->setLimit($limit);

        return $this;
    }

    /**
     * @param mixed $cols
     * @return $this
     */
    public function addCol($cols)
    {
        $this->getStatement()->addCol($cols);

        return $this;
    }

    /**
     * @param mixed $joins
     * @return $this
     */
    public function addJoin($joins)
    {
        $this->getStatement()->addJoin($joins);

        return $this;
    }

    /**
     * @param mixed $wheres
     * @return $this
     */
    public function addWhere($wheres)
    {
        $this->getStatement()->addWhere($wheres);

        return $this;
    }

    /**
     * @param mixed $groups
     * @return $this
     */
    public function addGroup($groups)
    {
        $this->getStatement()->addGroup($groups);

        return $this;
    }

    /* Statement filling methods */

    /**
     * @param array $filters
     * @return $this
     * @throws \Exception
     */
    public function addFilters(array $filters)
    {
        if (!method_exists($this, 'filter')) {
            throw new \Exception('Method filter not defined.');
        }

        foreach ($filters as $filterName => $filterValue) {
            $filter = $this->filter($filterName, $filterValue);

            if (array_key_exists('cols', $filter)) {
                $this->getStatement()
                    ->addCol($filter['cols']);
            }

            if (array_key_exists('joins', $filter)) {
                $this->getStatement()
                    ->addJoin($filter['joins']);
            }

            if (array_key_exists('where', $filter)) {
                $this->getStatement()
                    ->addWhere($filter['where']);
            }

            if (array_key_exists('binds', $filter)) {
                $this->getStatement()
                    ->addBindings($filter['binds']);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    protected function getDefaultSorterDir()
    {
        return 'ASC';
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function addDefaultSorter()
    {
        $dir = $this->getDefaultSorterDir();

        $this->addSorters(['default' => $dir]);

        return $this;
    }

    /**
     * @param array $sorters
     * @return $this
     * @throws \Exception
     */
    public function addSorters(array $sorters)
    {
        if (!method_exists($this, 'sorter')) {
            throw new \Exception(sprintf('Method sorter not defined in class %s.', get_called_class()));
        }

        $allowedDirs = [Statement::ORDER_ASC, Statement::ORDER_DESC];

        foreach ($sorters as $sorter => $dir) {
            if (!in_array(strtolower($dir), $allowedDirs)) {
                $sorter = $dir;
                $dir = Statement::ORDER_ASC;
            }

            $order = $this->sorter($sorter);

            $this->getStatement()
                ->addOrder($order, $dir);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (!method_exists($this, $name)) {

            $statement = $this->getStatement();
            $adapter = $this->getAdapter();

            if (method_exists($statement, $name)) {
                return call_user_func_array([$statement, $name], $arguments);
            } elseif (method_exists($adapter, $name)) {
                return call_user_func_array([$adapter, $name], $arguments);
            }
        }
    }
}
