<?php

/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 12.2.2016
 * Time: 15:35
 */
namespace Dense\Repository\Db\Statement;

trait StatementTrait
{
    /**
     * @var Statement
     */
    private $statement;

    /**
     * @var array
     */
    private $defaults = [
        'cols' => [],
        'joins' => [],
        'wheres' => [],
        'orders' => [],
    ];

    /**
     * @var string
     */
    protected $table;

    /**
     * @return Statement
     */
    public function getStatement()
    {
        if (!$this->hasStatement()) {
            $this->initStatement();
        }

        return $this->statement;
    }

    /**
     * @param Statement $statement
     * @return $this
     */
    public function setStatement(Statement $statement)
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasStatement()
    {
        return ($this->statement !== null);
    }

    /**
     * @return $this
     */
    protected function initStatement()
    {
        $this->setStatement(new Statement($this->table));

        return $this;
    }

    /**
     * @return $this
     */
    protected function destroyStatement()
    {
        $this->statement = null;

        return $this;
    }

    /* Defaults filling methods */

    /**
     * @param mixed $cols
     * @return $this
     */
    protected function addDefaultCol($cols)
    {
        if (!is_array($cols)) {
            $cols = [$cols];
        }

        foreach ($cols as $col) {
            $this->defaults['cols'][] = $col;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getDefaultCols()
    {
        return array_unique($this->defaults['cols']);
    }

    /**
     * @param mixed $joins
     * @return $this
     */
    protected function addDefaultJoin($joins)
    {
        if (!is_array($joins)) {
            $joins = [$joins];
        }

        foreach ($joins as $join) {
            $this->defaults['joins'][] = $join;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getDefaultJoins()
    {
        return array_unique($this->defaults['joins']);
    }

    /**
     * @param mixed $where
     * @return $this
     */
    protected function addDefaultWhere($wheres)
    {
        if (!is_array($wheres)) {
            $wheres = [$wheres];
        }

        foreach ($wheres as $where) {
            $this->defaults['wheres'][] = $where;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getDefaultWheres()
    {
        return array_unique($this->defaults['wheres']);
    }

    /**
     * @param mixed $orders
     * @return $this
     */
    protected function addDefaultOrder($orders)
    {
        if (!is_array($orders)) {
            $orders = [$orders];
        }

        foreach ($orders as $order) {
            $this->defaults['orders'][] = $order;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getDefaultOrders()
    {
        return array_unique($this->defaults['orders']);
    }

    /**
     * @return $this
     */
    protected function addSelectDefaults()
    {
        $statement = $this->getStatement();

        $statement
            ->addCol($this->getDefaultCols())
            ->addJoin($this->getDefaultJoins())
            ->addWhere($this->getDefaultWheres());

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (!method_exists($this, $name)) {

            $statement = $this->getStatement();

            if (method_exists($statement, $name)) {
                return call_user_func_array([$statement, $name], $arguments);
            }
        }
    }
}
