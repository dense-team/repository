<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 11.2.2016
 * Time: 23:44
 */

namespace Dense\Repository\Db\Statement;

class Statement
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var array
     */
    protected $cols = [];

    /**
     * @var array
     */
    protected $joins = [];

    /**
     * @var array
     */
    protected $wheres = [];

    /**
     * @var array
     */
    protected $groupBy = [];

    /**
     * @var array
     */
    protected $orderBy = [];

    /**
     * @var array
     */
    protected $bindings = [];

    const STMT_SELECT = 'select';
    const STMT_UPDATE = 'update';
    const STMT_INSERT = 'insert';
    const STMT_DELETE = 'delete';

    const ORDER_ASC = 'asc';
    const ORDER_DESC = 'desc';

    public function __construct($table)
    {
        $this->table = (string)$table;
    }

    /**
     * @bind int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = (int)$offset;

        return $this;
    }

    /**
     * @bind int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = (int)$limit;

        return $this;
    }

    /**
     * @param mixed $cols
     * @return $this
     */
    public function addCol($cols)
    {
        if (!is_array($cols)) {
            $cols = [$cols];
        }

        foreach ($cols as $col) {
            $this->cols[] = (string)$col;
        }

        return $this;
    }

    /**
     * @param mixed $joins
     * @return $this
     */
    public function addJoin($joins)
    {
        if (!is_array($joins)) {
            $joins = [$joins];
        }

        foreach ($joins as $join) {
            $this->joins[] = (string)$join;
        }

        return $this;
    }

    /**
     * @param mixed $wheres
     * @return $this
     */
    public function addWhere($wheres)
    {
        if (!is_array($wheres)) {
            $wheres = [$wheres];
        }

        foreach ($wheres as $where) {
            $this->wheres[] = (string)$where;
        }

        return $this;
    }

    /**
     * @param mixed $groups
     * @return $this
     */
    public function addGroup($groups)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }

        foreach ($groups as $group) {
            $this->groupBy[] = (string)$group;
        }

        return $this;
    }

    /**
     * @param string $order
     * @param string $dir
     * @return $this
     */
    public function setDefaultOrder($order, $dir = self::ORDER_ASC)
    {
        if (empty($this->orderBy)) {
            $this->addOrder($order, $dir);
        }

        return $this;
    }

    /**
     * @param string $order
     * @param string $dir
     * @return $this
     */
    public function addOrder($order, $dir = self::ORDER_ASC)
    {
        switch(strtolower($dir)){
            default:
            case self::ORDER_ASC:
                $dir = 'ASC';
                break;

            case self::ORDER_DESC:
                $dir = 'DESC';
                break;
        }

        $this->orderBy[] = (string)$order . ' ' . $dir;

        return $this;
    }

    /**
     * @param array $bindings
     * @return $this
     */
    public function addBindings(array $bindings)
    {
        foreach ($bindings as $bindingName => $bindingValue) {
            $this->bindings[(string)$bindingName] = $bindingValue;
        }

        return $this;
    }

    /* query making methods */

    /**
     * @return string
     */
    protected function makeCols()
    {
        if ($this->cols) {
            return implode(', ', array_unique($this->cols));
        }

        return '*';
    }

    /**
     * @return string
     */
    protected function makeJoins()
    {
        if ($this->joins) {
            return ' ' . implode(' ', array_unique($this->joins));
        }
    }

    /**
     * @return string
     */
    protected function makeWheres()
    {
        if ($this->wheres) {
            return ' WHERE ' . implode(' AND ', array_unique($this->wheres));
        }
    }

    /**
     * @return string
     */
    protected function makeGroupBy()
    {
        if ($this->groupBy) {
            return ' GROUP BY ' . implode(', ', array_unique($this->groupBy));
        }
    }

    /**
     * @return string
     */
    protected function makeOrderBy()
    {
        if ($this->orderBy) {
            return ' ORDER BY ' . implode(', ', array_unique($this->orderBy));
        }
    }

    /**
     * @return string
     */
    protected function makeLimit()
    {
        if ($this->limit) {
            return ' LIMIT ' . $this->limit;
        }
    }

    /**
     * @return string
     */
    protected function makeOffset()
    {
        if ($this->offset) {
            return ' OFFSET ' . $this->offset;
        }
    }

    /**
     * @return string
     */
    public function makeSelect()
    {
        $cols = $this->makeCols();

        $joins = $this->makeJoins();

        $wheres = $this->makeWheres();

        $groupBy = $this->makeGroupBy();

        $orderBy = $this->makeOrderBy();

        $limit = $this->makeLimit();

        $offset = $this->makeOffset();

        $sql = trim("SELECT {$cols} FROM {$this->table}{$joins}{$wheres}{$groupBy}{$orderBy}{$limit}{$offset}");

        return $sql;
    }

    /**
     * @param array $params
     * @return string
     */
    static public function makeUpdateBody(array $params)
    {
        $colsAndBinds = [];
        foreach ($params as $param) {
            $colsAndBinds[] = $param . ' = ' . ':'  . $param;
        }
        $colsAndBindsSql = implode(', ', $colsAndBinds);

        return "{$colsAndBindsSql}";
    }

    /**
     * @param array $columns
     * @return string
     */
    public function makeUpdate(array $columns)
    {
        $updateBodySql = self::makeUpdateBody($columns);

        $wheres = $this->makeWheres();

        $sql = trim("UPDATE {$this->table} SET {$updateBodySql}{$wheres}");

        return $sql;
    }

    /**
     * @param array $columns
     * @return string
     */
    static public function makeInsertBody(array $columns)
    {
        $colsSql = implode(', ', $columns);

        $binds = [];
        foreach ($columns as $column) {
            $binds[] = ':' . $column;
        }
        $bindsSql = implode(', ', $binds);

        return "({$colsSql}) VALUES ({$bindsSql})";
    }

    /**
     * @param array $columns
     * @return string
     */
    public function makeInsert(array $columns)
    {
        $insertBodySql = self::makeInsertBody($columns);

        $sql = trim("INSERT INTO {$this->table} {$insertBodySql}");

        return $sql;
    }

    /**
     * @param string $index
     * @return string
     */
    public function makeEmptyInsert($index)
    {
        $sql = trim("INSERT INTO {$this->table} ({$index}) VALUES (DEFAULT)");

        return $sql;
    }

    /**
     * @return string
     */
    public function makeDelete()
    {
        $wheres = $this->makeWheres();

        $sql = trim("DELETE FROM {$this->table}{$wheres}");

        return $sql;
    }

    /**
     * @return array
     */
    public function makeBindings()
    {
        return $this->bindings;
    }
}
