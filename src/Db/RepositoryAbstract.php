<?php

namespace Dense\Repository\Db;

use Illuminate\Support\Collection;

use Dense\Repository\RepositoryInterface;

abstract class RepositoryAbstract extends TableAbstract implements RepositoryInterface
{
    /**
     * @var string
     */
    protected $index;

    /**
     * @var array
     */
    protected $listCols = [];

    const FILTER_ID = 'filter_id';
    const EXCLUDE_ID = 'exclude_id';

    const SORTER_ID = 'sorter_id';

    public function __construct($modelClass, $resultType = null)
    {
        parent::__construct($modelClass, $resultType);

        $this->_init();
    }

    abstract protected function _init();

    /**
     * @param string $filterName
     * @param string $filterValue
     * @return array
     */
    protected function filter($filterName, $filterValue)
    {
        $filter = [];

        switch (strtolower($filterName)) {

            case self::FILTER_ID:
                $filter['where'][] = "{$this->table}.{$this->index} IN ({$this->quoteList($filterValue)})";
                break;

            case self::EXCLUDE_ID:
                $filter['where'][] = "{$this->table}.{$this->index} NOT IN ({$this->quoteList($filterValue)})";
                break;
        }

        return $filter;
    }

    /**
     * @return string
     */
    protected function sequence()
    {
        return "{$this->table}_{$this->index}_seq";
    }

    /*
     * Retrieving methods
     */

    public function find($id)
    {
        $this
            ->addCol("{$this->table}.*")
            ->addFilters([self::FILTER_ID => (int)$id]);

        list($sql, $binds) = $this->buildSelect();

        $data = $this->select($sql, $binds);

        if (!$data) {
            throw new \Exception('Položka neexistuje');
        }

        return $this->getResult($data);
    }

    public function all()
    {
        $this
            ->addCol($this->listCols)
            ->addDefaultSorter();

        list($sql, $binds) = $this->buildSelect();

        $data = $this->select($sql, $binds);

        return $this->getResult($data);
    }

    public function load($limit = null, $offset = null)
    {
        $this
            ->addCol($this->listCols)
            ->setLimit($limit)
            ->setOffset($offset)
            ->addDefaultSorter();

        list($sql, $binds) = $this->buildSelect();

        $data = $this->select($sql, $binds);

        return $this->getResult($data);
    }

    /*
     * Modifying mehods
     */

    public function save($mixedData)
    {
        if ($id = $this->getObjectId($mixedData)) {
            return $this->modify($mixedData);
        } else {
            return $this->create($mixedData);
        }
    }

    public function create($mixedData)
    {
        $data = $this->convertToArray($mixedData);
        $data = array_filter($data);
        unset($data[$this->index]);

        list($sql, $binds) = $this->buildInsert($data);
        $this->insert($sql, $binds);

        $id = $this->currval($this->sequence());
        $this->setObjectId($mixedData, $id);

        return $mixedData;
    }

    public function modify($mixedData)
    {
        $id = $this->getObjectId($mixedData);

        $data = $this->convertToArray($mixedData);
        unset($data[$this->index]);

        $this->addFilters([self::FILTER_ID => $id]);

        list($sql, $binds) = $this->buildUpdate($data);

        $this->update($sql, $binds);

        return $mixedData;
    }

    public function remove($mixedData)
    {
        $id = $this->getObjectId($mixedData);

        $this->addFilters([self::FILTER_ID => $id]);

        list($sql, $binds) = $this->buildDelete();

        $this->delete($sql, $binds);

        return $mixedData;
    }

    /* Helper methods */

    /**
     * @param mixed $mixedData
     * @return mixed
     */
    protected function getObjectId($mixedData)
    {
        if (is_object($mixedData)) {
            return $mixedData->{$this->index};
        } elseif (is_array($mixedData)) {
            return $mixedData[$this->index];
        }

        throw new \InvalidArgumentException('Invalid object or array argument.');
    }

    /**
     * @param mixed $mixedData
     * @param int $id
     * @return mixed
     */
    protected function setObjectId($mixedData, $id)
    {
        if (is_object($mixedData)) {
            $mixedData->{$this->index} = $id;

            return $this;
        } elseif (is_array($mixedData)) {
            $mixedData[$this->index] = $id;

            return $this;
        }

        throw new \InvalidArgumentException('Invalid object or array argument.');
    }

    /**
     * @param $mixedData
     * @param bool $filtered
     * @return array
     */
    protected function convertToArray($mixedData)
    {
        if (is_array($mixedData)) {
            $data = $mixedData;
        } elseif (is_object($mixedData)) {
            if (method_exists($mixedData, 'toPersist')) {
                $data = $mixedData->toPersist();
            } elseif (method_exists($mixedData, 'toArray')) {
                $data = $mixedData->toArray();
            } else {
                throw new \InvalidArgumentException('Invalid arrayable object argument.');
            }
        } elseif (is_scalar($mixedData)) {
            $data = [$mixedData];
        } else {
            throw new \InvalidArgumentException('Invalid database manipulation argument.');
        }

        return $data;
    }

    /**
     * @param mixed $mixedObjects
     * @return Collection
     */
    protected function convertToCollection($mixedObjects)
    {
        if (is_array($mixedObjects)) {
            $objects = new Collection($mixedObjects);
        } elseif ($mixedObjects instanceof Collection) {
            $objects = $mixedObjects;
        } else {
            throw new \InvalidArgumentException('Invalid collection of objects argument.');
        }

        $objects = $objects->keyBy($this->index);

        return $objects;
    }
}