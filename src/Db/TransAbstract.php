<?php

namespace Dense\Repository\Db;

use Dense\Repository\Db\Statement\Statement;

abstract class TransAbstract extends RepositoryAbstract
{
    /**
     * @var string
     */
    protected $transTable;

    /**
     * @var array
     */
    protected $transCols = [];

    public function __construct($modelClass, $resultType = null)
    {
        parent::__construct($modelClass, $resultType);

        $this
            ->addDefaultCol($this->transCols)
            ->addDefaultJoin("LEFT JOIN {$this->transTable} ON ({$this->transTable}.{$this->index} = {$this->table}.{$this->index} AND {$this->transTable}.lang = " . $this->quote(content_lang()) . ")");
    }

    /**
     * @param array $data
     * @return array
     */
    protected function parseData(array $data)
    {
        $generalData = [];
        $transData = [
            'lang' => content_lang(),
        ];

        foreach ($data as $propName => $propValue) {
            if (in_array($propName, $this->transCols)) {
                $transData[$propName] = $propValue;
            } else{
                $generalData[$propName] = $propValue;
            }
        }

        return [$generalData, $transData];
    }

    /**
     * @param mixed $objectOrArray
     * @return mixed
     * @throws \Exception
     */
    public function modify($objectOrArray)
    {
        $this->beginTransaction();

        try {
            $data = $this->convertToArray($objectOrArray);
            list($generalData, $transData) = $this->parseData($data);

            // modify general table
            $statement = new Statement($this->table);
            $statement->addWhere([
                "{$this->index} = :{$this->index}",
            ]);

            $generalCols = array_keys($generalData);
            if (($indexKey = array_search($this->index, $generalCols)) !== false) {
                unset($generalCols[$indexKey]);
            }
            $modifySql = $statement->makeUpdate($generalCols);

            $this->update($modifySql, $generalData);
            unset($statement);

            // clear translations in trans table based on object id and language
            $statement = new Statement($this->transTable);
            $statement->addWhere([
                "{$this->index} = :{$this->index}",
                "lang = :lang",
            ]);
            $clearSql = $statement->makeDelete();
            $this->delete($clearSql, [
                $this->index => $this->getObjectId($objectOrArray),
                'lang' => content_lang(),
            ]);
            unset($statement);

            // set existing id from general data table
            $transData = array_merge($transData, [
                $this->index => $data[$this->index],
            ]);

            // create translations in trans table
            $statement = new Statement($this->transTable);
            $transSql = $statement->makeInsert(array_keys($transData));
            $this->insert($transSql, $transData);
            unset($statement);

            $this->commit();
        } catch (\Exception $e) {
            $this->rollBack();

            throw $e;
        }

        return $objectOrArray;
    }

    /**
     * @param mixed $objectOrArray
     * @return mixed
     * @throws \Exception
     */
    public function create($objectOrArray)
    {
        $this->beginTransaction();

        try {
            $data = $this->convertToArray($objectOrArray, true);
            list($generalData, $transData) = $this->parseData($data);

            $statement = new Statement($this->table);
            if (empty($generalData)) {
                $createSql = $statement->makeEmptyInsert($this->index);
            } else {
                $generalCols = array_keys($generalData);
                $createSql = $statement->makeInsert($generalCols);
            }

            $this->insert($createSql, $generalData);
            unset($statement);

            $id = $this->currval($this->sequence());
            $this->setObjectId($objectOrArray, $id);

            // set new id from general data table
            $transData = array_merge($transData, [
                $this->index => $id,
            ]);

            // create translations in trans table
            $statement = new Statement($this->transTable);
            $transSql = $statement->makeInsert(array_keys($transData));
            $this->insert($transSql, $transData);
            unset($statement);

            $this->commit();
        } catch (\Exception $e) {
            $this->rollBack();

            throw $e;
        }

        return $objectOrArray;
    }

}