<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 26.6.2016
 * Time: 13:07
 */

namespace Dense\Repository\Db\Profiler;

class Bag
{
    protected $time;

    protected $query;

    protected $binds;

    public function __construct($time, $query, array $binds)
    {
        $this->time = $time;

        $this->query = $query;

        $this->binds = $binds;
    }

    /**
     * @return string
     */
    private function printTime(){
        return round($this->time * 1000, 3) . ' ms';
    }

    /**
     * @return string
     */
    private function printQuery(){
        return addslashes(preg_replace('/\s+/', ' ', $this->query));
    }

    /**
     * @return string
     */
    private function printBinds(){
        if (!empty($this->binds)) {

            return json_encode($this->binds);
        }

        return ' - no binds - ';
    }

    /**
     * @return void
     */
    public function printToConsole()
    {
        $log =
            'console.log(\'%s\');' .
            'console.log(\'%s\');' .
            'console.log(\'%s\');' .
            'console.log(\'----------\');';

        printf($log, $this->printTime(), $this->printQuery(), $this->printBinds());
    }
}
