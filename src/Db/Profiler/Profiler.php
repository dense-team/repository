<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 3.7.2016
 * Time: 17:20
 */

namespace Dense\Repository\Db\Profiler;

class Profiler
{
    /**
     * @var Profiler
     */
    private static $instance;

    /**
     * @var array
     */
    private $info = [];

    private function __construct() {}

    private function __clone() {}

    /**
     * @return Profiler
     */
    static public function getInstance()
    {
        if (static::$instance === null) {

            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param Bag $bag
     * @return $this
     */
    public function addInfo(Bag $bag)
    {
        $this->info[] = $bag;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasInfo()
    {
        return !empty($this->info);
    }

    /**
     * @return void
     */
    public function printToConsole()
    {
        if ($this->hasInfo()) {
            echo '<script>';
            foreach ($this->info as $bag) {
                $bag->printToConsole();
            }
            echo '</script>';
        }
    }
}
