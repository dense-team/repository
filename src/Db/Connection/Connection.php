<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 26.6.2016
 * Time: 21:15
 */

namespace Dense\Repository\Db\Connection;

use Dense\Repository\Db;

class Connection
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var array
     */
    private static $config = [];

    /**
     * @var string
     */
    private static $env = 'prod';

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @param string $connection
     * @throws \Exception
     */
    private function __construct($connection)
    {
        $this->initInstance($connection);
    }

    private function __clone()
    {
    }

    /**
     * @param array $config
     */
    static public function setConfig(array $config)
    {
        $allowedConfig = ['default', 'connections'];

        self::$config = array_filter($config, function ($key) use ($allowedConfig) {
            return in_array($key, $allowedConfig);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param array $config
     */
    static public function setEnv($env)
    {
        self::$env = (string)$env;
    }

    /**
     * @return bool
     */
    static public function isProdEnv()
    {
        return (self::$env === 'prod');
    }

    /**
     * @param string $connection
     * @param string $value
     * @return mixed
     */
    static private function getConnValue($connection, $value)
    {
        return self::$config['connections'][$connection][$value];
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @param string $connection
     * @return string
     */
    static public function getConnection($connection)
    {
        if (!isset(self::$config['connections'][$connection])) {
            $connection = self::$config['default'];
        }

        return $connection;
    }

    /**
     * @param string $connection
     * @return $this
     */
    static public function getInstance($connection = 'default')
    {
        $connection = self::getConnection($connection);

        if (!isset(self::$instances[$connection])) {
            self::$instances[$connection] = new static($connection);
        }

        return self::$instances[$connection];
    }

    /**
     * @param $connection
     * @throws \Exception
     */
    private function initInstance($connection)
    {
        $driver = self::getConnValue($connection, 'driver');

        switch (strtolower($driver)) {
            case 'pgsql':
                $this->pdo = $this->initPgsqlConnection($connection);
                break;

            case 'dblib':
                $this->pdo = $this->initDblibConnection($connection);
                break;

            case 'sqlsrv':
                $this->pdo = $this->initSqlsrvConnection($connection);
                break;

            case 'mock':
                $this->pdo = $this->initMockConnection();
                break;

            default:
                throw new \Exception('Unknown database driver');
                break;
        }

        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

        if (!self::isProdEnv()) {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
    }

    /**
     * @param string $connection
     * @return string
     */
    public function getPgsqlDsn($connection)
    {
        return self::getConnValue($connection, 'driver') . ':' . implode(';', [
            'host=' . self::getConnValue($connection, 'host'),
            'dbname=' . self::getConnValue($connection, 'database'),
            'port=' . self::getConnValue($connection, 'port'),
        ]);
    }

    /**
     * @param string $connection
     * @return \PDO
     */
    public function initPgsqlConnection($connection)
    {
        $dsn = $this->getPgsqlDsn($connection);

        $username = self::getConnValue($connection, 'username');
        $password = self::getConnValue($connection, 'password');

        $pdo = new \PDO($dsn, $username, $password);

        $schema = self::getConnValue($connection, 'schema');
        if ($schema !== 'public') {
            $pdo->exec("SET search_path TO {$schema}, public");
        }

        return $pdo;
    }

    /**
     * @param string $connection
     * @return string
     */
    public function getDblibDsn($connection)
    {
        return self::getConnValue($connection, 'driver') . ':' . implode(';', [
            'host=' . self::getConnValue($connection, 'host') . ':' . self::getConnValue($connection, 'port'),
            'dbname=' . self::getConnValue($connection, 'database'),
        ]);
    }

    /**
     * @param string $connection
     * @return \PDO
     */
    public function initDblibConnection($connection)
    {
        $dsn = $this->getDblibDsn($connection);

        $username = self::getConnValue($connection, 'username');
        $password = self::getConnValue($connection, 'password');

        $pdo = new \PDO($dsn, $username, $password);

        return $pdo;
    }

    /**
     * @param string $connection
     * @return string
     */
    public function getSqlsrvDsn($connection)
    {
        return self::getConnValue($connection, 'driver') . ':' . implode(';', [
            'Server=' . self::getConnValue($connection, 'host') . ',' . self::getConnValue($connection, 'port'),
            'Database=' . self::getConnValue($connection, 'database'),
        ]);
    }

    /**
     * @param string $connection
     * @return \PDO
     */
    public function initSqlsrvConnection($connection)
    {
        $dsn = $this->getSqlsrvDsn($connection);

        $username = self::getConnValue($connection, 'username');
        $password = self::getConnValue($connection, 'password');

        $pdo = new \PDO($dsn, $username, $password);

        return $pdo;
    }

    /**
     * @return string
     */
    public function getMockDsn()
    {
        return 'sqlite::memory:';
    }

    /**
     * @return \PDO
     */
    public function initMockConnection()
    {
        $dsn = $this->getMockDsn();

        $pdo = new \PDO($dsn);

        return $pdo;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (!method_exists($this, $name)) {

            if (method_exists($this->pdo, $name)) {
                return call_user_func_array([$this->pdo, $name], $arguments);
            }
        }
    }
}
