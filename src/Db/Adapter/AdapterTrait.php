<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 12.2.2016
 * Time: 14:43
 */

namespace Dense\Repository\Db\Adapter;

use Dense\Repository\Db\Connection\Connection;

trait AdapterTrait
{
    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @return Adapter
     */
    public function getAdapter()
    {
        if (!$this->hasAdapter()) {
            $this->initAdapter();
        }

        return $this->adapter;
    }

    /**
     * @param Adapter $adapter
     * @return $this
     */
    public function setAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAdapter()
    {
        return ($this->adapter !== null);
    }

    /**
     * @return $this
     */
    protected function initAdapter()
    {
        $driver = $this->getDriver();

        $connection = Connection::getInstance($driver);
        $pdo = $connection->getPdo();

        $this->setAdapter(new Adapter($pdo));

        return $this;
    }

    /**
     * @return string
     */
    abstract protected function getDriver();

    /**
     * @param string $string
     * @return string
     */
    public function quote($string)
    {
        return $this->getAdapter()->quote($string);
    }

    /**
     * @param mixed $data
     * @return string
     */
    public function quoteList($data)
    {
        return $this->getAdapter()->quoteList($data);
    }

    /**
     * @param string $sequence
     * @return int
     * @throws \Exception
     */
    public function currval($sequence)
    {
        return $this->getAdapter()->currval($sequence);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function select($sql, array $binds = [])
    {
        return $this->getAdapter()->select($sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function insert($sql, array $binds = [])
    {
        return $this->getAdapter()->insert($sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function update($sql, array $binds = [])
    {
        return $this->getAdapter()->update($sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function delete($sql, array $binds = [])
    {
        return $this->getAdapter()->delete($sql, $binds);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (!method_exists($this, $name)) {

            $adapter = $this->getAdapter();

            if (method_exists($adapter, $name)) {
                return call_user_func_array([$adapter, $name], $arguments);
            }
        }
    }
}