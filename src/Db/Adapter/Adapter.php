<?php

namespace Dense\Repository\Db\Adapter;

use Dense\Repository\Db\Connection\Connection;
use Dense\Repository\Db\Profiler\Bag;
use Dense\Repository\Db\Profiler\Profiler;

class Adapter
{
    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $string
     * @return string
     */
    public function quote($string)
    {
        return $this->connection->quote($string);
    }

    /**
     * @param mixed $data
     * @return string
     */
    public function quoteList($data)
    {
        if(is_scalar($data)){
            $data = [$data];
        }

        if (is_array($data)) {
            $data = array_unique(array_filter($data));

            if (!empty($data)) {
                $quotables = [];
                foreach ($data as $string) {
                    $quotables[] = $this->connection->quote($string);
                }

                unset($data);
                return implode(', ', $quotables);
            }
        }

        return 'NULL';
    }

    /**
     * @param string $sequence
     * @return int
     * @throws \Exception
     */
    public function currval($sequence)
    {
        $sql = "SELECT currval('{$sequence}') AS currval";
        $data = $this->makeDbRequest('select', $sql);

        $row = reset($data);
        $id = $row['currval'];

        if (!$id) {
            throw new \Exception(sprintf('Wrong currval result for sequence %s', $sequence));
        }

        return $id;
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function select($sql, array $binds = [])
    {
        return $this->makeDbRequest('select', $sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function insert($sql, array $binds = [])
    {
        return $this->makeDbRequest('insert', $sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function update($sql, array $binds = [])
    {
        return $this->makeDbRequest('update', $sql, $binds);
    }

    /**
     * @param string $sql
     * @param array $binds
     * @return array
     */
    public function delete($sql, array $binds = [])
    {
        return $this->makeDbRequest('delete', $sql, $binds);
    }

    /**
     * @param string $type
     * @param string $sql
     * @param array $binds
     * @return array
     */
    protected function makeDbRequest($type, $sql, array $binds = [])
    {
        $data = [];

        $starttime = microtime(true);

        switch(strtolower($type)){
            case 'select':
                $stmt = $this->connection->prepare($sql);
                $stmt->execute($binds);

                $data = $stmt->fetchAll();

                break;

            case 'insert':
            case 'update':
            case 'delete':
                $stmt = $this->connection->prepare($sql);

                foreach ($binds as $bindName => $bindValue) {

                    $bindType = \PDO::PARAM_STR;

                    if (is_int($bindValue)) {
                        $bindType = \PDO::PARAM_INT;
                    } elseif (is_bool($bindValue)) {
                        $bindType = \PDO::PARAM_BOOL;
                    } elseif (is_null($bindValue)) {
                        $bindType = \PDO::PARAM_NULL;
                    }

                    $stmt->bindValue($bindName, $bindValue, $bindType);
                }
                $stmt->execute();

                break;

            default:
                throw new \InvalidArgumentException('Invalid query type');

                break;
        }

        $endtime = microtime(true);

        if ($this->canProfile()) {
            $info = new Bag($endtime - $starttime, $sql, $binds);

            $profiler = Profiler::getInstance();
            $profiler->addInfo($info);
        }

        return $data;
    }

    /**
     * @return bool
     */
    protected function canProfile()
    {
        return !Connection::isProdEnv();
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (method_exists($this->connection, $name)) {
            return call_user_func_array([$this->connection, $name], $arguments);
        }
    }
}
