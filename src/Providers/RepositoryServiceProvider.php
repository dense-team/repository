<?php

namespace Dense\Repository\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

use Dense\Repository\Db\Connection\Connection;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        //$configDir = __DIR__ . '/../../config';

        //$this->mergeConfigFrom($configDir . '/repository.php', 'repository');
    }

    public function register()
    {
        Connection::setConfig(Config::get('database'));
        Connection::setEnv(App::environment());
    }
}
