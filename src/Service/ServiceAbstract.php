<?php

namespace Dense\Repository\Service;

use Dense\Repository;

abstract class ServiceAbstract{

    use \Dense\Repository\Result\ResultTrait;

    protected $httpClient;

    public function __construct($httpClient, $modelClass) {
        $this->setResultPrototype($modelClass);
        
        $this->httpClient = $httpClient;
    }

    protected function serviceCredentials() {
        return [config('services.dense.login'), config('services.dense.password')];
    }
        
}