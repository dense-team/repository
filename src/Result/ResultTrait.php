<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 21.8.2016
 * Time: 10:24
 */

namespace Dense\Repository\Result;

use Illuminate\Support\Collection;

trait ResultTrait
{
    /**
     * @var object
     */
    private $resultPrototype;

    /**
     * @var string
     */
    private $resultType = 'MODEL';

    /**
     * @return object
     */
    public function getResultPrototype()
    {
        return $this->resultPrototype;
    }

    /**
     * @param object $resultPrototype
     * @return $this
     */
    public function setResultPrototype($resultPrototype)
    {
        $this->resultPrototype = $resultPrototype;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultType()
    {
        return $this->resultType;
    }

    /**
     * @param string $resultType
     * @return $this
     */
    public function setResultType($resultType)
    {
        $this->resultType = $resultType;

        return $this;
    }

    /**
     * @param array $data
     * @return string
     * @throws \Exception
     */
    protected function getResultClone(array $data)
    {
        $resultPrototype = $this->getResultPrototype();

        switch(strtoupper($this->resultType)){

            case 'MODEL':
                $resultClone = clone $resultPrototype;
                $resultClone->exchangeArray($data);
                break;

            case 'FACTORY':
                $resultClone = $resultPrototype::create($data);
                break;

            default:
                throw new \Exception('Invalid result type.');
                break;
        }

        return $resultClone;
    }

    /**
     * @param array $data
     * @return Collection
     * @throws \Exception
     */
    public function getResult(array $data)
    {
        $index = null;
        if (isset($this->index)) {
            $index = $this->index;
        }

        $collection = new Collection();

        foreach ($data as $idx => $item) {

            $result = $this->getResultClone($item);

            if (!is_null($index) && array_key_exists($index, $item)) {
                $idx = $item[$index];

                $collection->put($idx, $result);
            } else{
                $collection->push($result);
            }
        }

        return $collection;
    }

    /**
     * @param object $resultPrototype
     * @param string $resultType
     */
    protected function initResult($resultPrototype, $resultType = 'MODEL')
    {
        $this->setResultPrototype($resultPrototype);

        if (!is_null($resultType)) {
            $this->setResultType($resultType);
        }
    }

}
