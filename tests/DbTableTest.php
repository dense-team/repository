<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 8.4.2016
 * Time: 15:54
 */

use Dense\Repository\Db\TableAbstract;

class DbTableTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getTable()
    {
        $pdo = new \PDO('sqlite::memory:');

        $adapter = new Dense\Repository\Db\Adapter\Adapter($pdo, false);

        $testTable = Mockery::mock('TestTable[getAdapter]', [new stdClass()]);
        $testTable->shouldReceive('getAdapter')->andReturn($adapter);

        return $testTable;
    }

    // select
    public function testSelect()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->addWhere("id = :id");

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT * FROM test WHERE id = :id";
        $this->assertEquals($sql, $expected);

        $this->assertFalse($testTable->hasStatement());
    }

    public function testSelectWithCustomCols()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->addCol(['col1', 'col2', 'col3']);

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT col1, col2, col3 FROM test";
        $this->assertEquals($sql, $expected);

        $this->assertFalse($testTable->hasStatement());
    }

    public function testSelectWithLimit()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->setLimit(100);

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT * FROM test LIMIT 100";
        $this->assertEquals($sql, $expected);
    }

    public function testSelectWithOffset()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->setOffset(100);

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT * FROM test OFFSET 100";
        $this->assertEquals($sql, $expected);
    }

    public function testSelectWithOrders()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->addOrder("id");

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT * FROM test ORDER BY id ASC";
        $this->assertEquals($sql, $expected);

        $testTable->getStatement()
                ->addOrder("col1")
                ->addOrder("col2", 'aSc')
                ->addOrder("col3", 'dESc');

        list($sql, $binds) = $testTable->buildSelect();

        $expected = "SELECT * FROM test ORDER BY col1 ASC, col2 ASC, col3 DESC";
        $this->assertEquals($sql, $expected);
    }

    // update
    public function testUpdate()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->addWhere("id = :id");

        list($sql, $binds) = $testTable->buildUpdate([
            'col1' => 1,
            'col2' => 2,
            'col3' => 3,
        ]);

        $expected = "UPDATE test SET col1 = :col1, col2 = :col2, col3 = :col3 WHERE id = :id";
        $this->assertEquals($sql, $expected);

        $this->assertFalse($testTable->hasStatement());
    }

    // insert
    public function testInsert()
    {
        $testTable = $this->getTable();

        list($sql, $binds) = $testTable->buildInsert([
            'col1' => 1,
            'col2' => 2,
            'col3' => 3,
        ]);

        $expected = "INSERT INTO test (col1, col2, col3) VALUES (:col1, :col2, :col3)";
        $this->assertEquals($sql, $expected);

        $this->assertFalse($testTable->hasStatement());
    }

    // delete
    public function testDelete()
    {
        $testTable = $this->getTable();

        $testTable->getStatement()
            ->addWhere("id = :id");

        list($sql, $binds) = $testTable->buildDelete();

        $expected = "DELETE FROM test WHERE id = :id";
        $this->assertEquals($sql, $expected);

        $this->assertFalse($testTable->hasStatement());
    }
}

class TestTable extends TableAbstract
{
    protected $table = 'test';
}
