<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.1.2017
 * Time: 21:32
 */

use Dense\Repository\Db\Connection\Connection;

class DbConnectionTest extends PHPUnit_Framework_TestCase
{
    protected function getConnection($connection = null)
    {
        Connection::setConfig([
            'default' => 'test',
            'connections' => [
                'test' => [
                    'driver'   => 'mock',
                ],
                'pgsql' => [
                    'driver'   => 'mock',
                    'host'     => '127.0.0.1',
                    'database' => 'test_db',
                    'username' => 'test_username',
                    'password' => 'test_password',
                    'charset'  => 'utf8',
                    'schema'   => 'public',
                    'port'     => '5432',
                    'prefix'   => '',
                    'sslmode'  => 'prefer',
                ],
                'sqlsrv' => [
                    'driver'   => 'mock',
                    'host'     => '127.0.0.1',
                    'database' => 'test_db',
                    'username' => 'test_username',
                    'password' => 'test_password',
                    'charset'  => 'utf8',
                    'port'     => '1542',
                    'prefix'   => '',
                ],
            ],
        ]);
        Connection::setEnv('prod');

        return Connection::getInstance($connection);
    }

    public function testPgsqlDsn()
    {
        $testConnection = $this->getConnection();

        $dsn = $testConnection->getPgsqlDsn('pgsql');

        $expected = 'mock:host=127.0.0.1;dbname=test_db;port=5432';

        $this->assertEquals($dsn, $expected);
    }

    public function testDblibDsn()
    {
        $testConnection = $this->getConnection();

        $dsn = $testConnection->getDblibDsn('sqlsrv');

        $expected = 'mock:host=127.0.0.1:1542;dbname=test_db';

        $this->assertEquals($dsn, $expected);
    }

    public function testSqlsrvDsn()
    {
        $testConnection = $this->getConnection();

        $dsn = $testConnection->getSqlsrvDsn('sqlsrv');

        $expected = 'mock:Server=127.0.0.1,1542;Database=test_db';

        $this->assertEquals($dsn, $expected);
    }

    public function testMockDsn()
    {
        $testConnection = $this->getConnection();

        $dsn = $testConnection->getMockDsn();

        $expected = 'sqlite::memory:';

        $this->assertEquals($dsn, $expected);
    }

    public function testConnectionConfigValue()
    {
        $testConnection = $this->getConnection();

        $connection = $testConnection::getConnection('default');

        $expected = 'test';

        $this->assertEquals($connection, $expected);
    }
}