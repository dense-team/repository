<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.4.2016
 * Time: 21:34
 */

use Dense\Repository\Db\Statement\Statement;

class DbStatementTest extends PHPUnit_Framework_TestCase
{
    protected function getStatement()
    {
        $testStatement = new Statement('test');

        return $testStatement;
    }

    public function testMakeSelect()
    {
        $testStatement = $this->getStatement();

        $testStatement
            ->addCol('id')
            ->addCol(['col1', 'col2', 'col3'])
            ->addJoin("JOIN inner_table ON(inner_table.id = test.id)")
            ->addJoin([
                "LEFT JOIN left_join_table ON(left_join_table.id = test.id)",
                "RIGHT JOIN right_join_table ON(right_join_table.id = test.id)",
            ])
            ->addWhere("id = :id")
            ->addWhere([
                "col1 = :col1",
                "col2 = :col2",
                "col3 = :col3",
            ])
            ->addGroup('id')
            ->addGroup(['col1', 'col2', 'col3'])
            ->addOrder('id')
            ->setLimit(100)
            ->setOffset(100);

        $sql = $testStatement->makeSelect();

        $expected = "SELECT id, col1, col2, col3 FROM test " .
            "JOIN inner_table ON(inner_table.id = test.id) " .
            "LEFT JOIN left_join_table ON(left_join_table.id = test.id) " .
            "RIGHT JOIN right_join_table ON(right_join_table.id = test.id) " .
            "WHERE id = :id AND col1 = :col1 AND col2 = :col2 AND col3 = :col3 " .
            "GROUP BY id, col1, col2, col3 " .
            "ORDER BY id ASC " .
            "LIMIT 100 OFFSET 100";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeSelectWithUniqueQueries()
    {
        $testStatement = $this->getStatement();

        $testStatement
            ->addCol('id')
            ->addCol('id')
            ->addJoin("INNER JOIN inner_table ON(inner_table.id = test.id)")
            ->addJoin("INNER JOIN inner_table ON(inner_table.id = test.id)")
            ->addWhere("id = :id")
            ->addWhere("id = :id")
            ->addGroup('id')
            ->addGroup('id')
            ->addOrder('id')
            ->addOrder('id');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT id FROM test " .
            "INNER JOIN inner_table ON(inner_table.id = test.id) " .
            "WHERE id = :id " .
            "GROUP BY id " .
            "ORDER BY id ASC";

        $this->assertEquals($sql, $expected);
    }

    public function testNonIntegerLimit()
    {
        $testStatement = $this->getStatement();

        $testStatement->setLimit('string');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT * FROM test";

        $this->assertEquals($sql, $expected);
    }

    public function testNonIntegerOffset()
    {
        $testStatement = $this->getStatement();

        $testStatement->setOffset('string');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT * FROM test";

        $this->assertEquals($sql, $expected);
    }

    public function testDefaultDirForOrder()
    {
        $testStatement = $this->getStatement();

        $testStatement->addOrder('id');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT * FROM test ORDER BY id ASC";

        $this->assertEquals($sql, $expected);
    }

    public function testAscDirForOrder()
    {
        $testStatement = $this->getStatement();

        $testStatement->addOrder('id', 'aSc');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT * FROM test ORDER BY id ASC";

        $this->assertEquals($sql, $expected);
    }

    public function testDescDirForOrder()
    {
        $testStatement = $this->getStatement();

        $testStatement->addOrder('id', 'dESc');

        $sql = $testStatement->makeSelect();

        $expected = "SELECT * FROM test ORDER BY id DESC";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeUpdateBody()
    {
        $sql = Statement::makeUpdateBody(['col1', 'col2', 'col3']);

        $expected = "col1 = :col1, col2 = :col2, col3 = :col3";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeUpdate()
    {
        $testStatement = $this->getStatement();

        $testStatement->addWhere("id = :id");
        $sql = $testStatement->makeUpdate(['col1', 'col2', 'col3']);

        $expected = "UPDATE test SET col1 = :col1, col2 = :col2, col3 = :col3 WHERE id = :id";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeInsertBody()
    {
        $sql = Statement::makeInsertBody(['col1', 'col2', 'col3']);

        $expected = "(col1, col2, col3) VALUES (:col1, :col2, :col3)";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeInsert()
    {
        $testStatement = $this->getStatement();

        $sql = $testStatement->makeInsert(['col1', 'col2', 'col3']);

        $expected = "INSERT INTO test (col1, col2, col3) VALUES (:col1, :col2, :col3)";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeEmptyInsert()
    {
        $testStatement = $this->getStatement();

        $sql = $testStatement->makeEmptyInsert('id');

        $expected = "INSERT INTO test (id) VALUES (DEFAULT)";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeDelete()
    {
        $testStatement = $this->getStatement();
        $testStatement->addWhere("id = :id");

        $sql = $testStatement->makeDelete();

        $expected = "DELETE FROM test WHERE id = :id";

        $this->assertEquals($sql, $expected);
    }

    public function testMakeBindings()
    {
        $testStatement = $this->getStatement();
        $testStatement
            ->addBindings([
                'col1' => 1,
                'col2' => 2,
                'col3' => 3,
            ]);

        $sql = $testStatement->makeBindings();

        $expected = [
            'col1' => 1,
            'col2' => 2,
            'col3' => 3,
        ];

        $this->assertEquals($sql, $expected);
    }

    public function testMakeBindingsWithUniqueBinds()
    {
        $testStatement = $this->getStatement();
        $testStatement
            ->addBindings(['id' => 1])
            ->addBindings(['id' => 1]);

        $sql = $testStatement->makeBindings();

        $expected = [
            'id' => 1,
        ];

        $this->assertEquals($sql, $expected);
    }
}