<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 11.4.2016
 * Time: 21:16
 */

use Dense\Repository\Db\RepositoryAbstract;

class DbStorageTest extends PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        Mockery::close();
    }

    protected function getStorage()
    {
        $pdo = new \PDO('sqlite::memory:');

        $adapter = new Dense\Repository\Db\Adapter\Adapter($pdo, false);

        $testStorage = Mockery::mock('TestStorage[getAdapter]', [new stdClass()]);
        $testStorage->shouldReceive('getAdapter')->andReturn($adapter);

        return $testStorage;
    }

    public function testSelectWithDefaultFilters()
    {
        $testStorage = $this->getStorage();

        $testStorage
            ->addFilters([$testStorage::FILTER_ID => 1]);

        list($sql, $binds) = $testStorage->buildSelect();

        $expected = "SELECT * FROM test WHERE test.id IN ('1')";
        $this->assertEquals($sql, $expected);

        $testStorage
            ->addFilters([$testStorage::EXCLUDE_ID => 1]);

        list($sql, $binds) = $testStorage->buildSelect();

        $expected = "SELECT * FROM test WHERE test.id NOT IN ('1')";
        $this->assertEquals($sql, $expected);
    }

    public function testSelectWithFilters()
    {
        $testStorage = $this->getStorage();

        $testStorage
            ->addFilters([
                $testStorage::FILTER_COL1 => [1, 2, 3],
                $testStorage::FILTER_COL2 => [1, 2, 3],
                $testStorage::FILTER_COL3 => [1, 2, 3],
            ]);

        list($sql, $binds) = $testStorage->buildSelect();

        $expected =
            "SELECT * FROM test " .
            "WHERE test.col1 IN ('1', '2', '3') " .
            "AND test.col2 IN ('1', '2', '3') " .
            "AND test.col3 IN ('1', '2', '3')";

        $this->assertEquals($sql, $expected);
    }

    public function testSelectWithDefaultSorter()
    {
        $testStorage = $this->getStorage();

        $testStorage
            ->addDefaultSorter();

        list($sql, $binds) = $testStorage->buildSelect();

        $expected = "SELECT * FROM test ORDER BY test.id ASC";
        $this->assertEquals($sql, $expected);
    }

    public function testSelectWithSorter()
    {
        $testStorage = $this->getStorage();

        $testStorage
            ->addSorters([
                $testStorage::SORTER_COL1,
                $testStorage::SORTER_COL2 => 'aSc',
                $testStorage::SORTER_COL3 => 'dESc',
            ]);

        list($sql, $binds) = $testStorage->buildSelect();

        $expected = "SELECT * FROM test ORDER BY test.col1 ASC, test.col2 ASC, test.col3 DESC";
        $this->assertEquals($sql, $expected);
    }
}

class TestStorage extends RepositoryAbstract
{
    protected $table = 'test';
    protected $index = 'id';

    const SORTER_ID = 'sorter_id';
    const SORTER_COL1 = 'sorter_col1';
    const SORTER_COL2 = 'sorter_col2';
    const SORTER_COL3 = 'sorter_col3';

    const FILTER_COL1 = 'filter_col1';
    const FILTER_COL2 = 'filter_col2';
    const FILTER_COL3 = 'filter_col3';

    protected function _init()
    {
        $this->listCols = [
            "{$this->table}.{$this->index}",
            "{$this->table}.col1",
            "{$this->table}.col2",
            "{$this->table}.col3",
        ];
    }

    protected function sorter($sorterName)
    {
        switch (strtolower($sorterName)) {

            default:
            case self::SORTER_ID:
                $sorter = "{$this->table}.{$this->index}";
                break;

            case self::SORTER_COL1:
                $sorter = "{$this->table}.col1";
                break;

            case self::SORTER_COL2:
                $sorter = "{$this->table}.col2";
                break;

            case self::SORTER_COL3:
                $sorter = "{$this->table}.col3";
                break;
        }

        return $sorter;
    }

    protected function filter($filterName, $filterValue)
    {
        $filter = parent::filter($filterName, $filterValue);

        switch (strtolower($filterName)) {

            case self::FILTER_COL1:
                $filter['where'][] = "{$this->table}.col1 IN (". $this->quoteList($filterValue) .")";
                break;

            case self::FILTER_COL2:
                $filter['where'][] = "{$this->table}.col2 IN (". $this->quoteList($filterValue) .")";
                break;

            case self::FILTER_COL3:
                $filter['where'][] = "{$this->table}.col3 IN (". $this->quoteList($filterValue) .")";
                break;
        }

        return $filter;
    }
}
